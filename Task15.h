#pragma once

typedef struct PrimeFactor
{
	int factor;
	int count;
}PrimeFactor;

void getRawFactors(int n, int** factors, int* factorsCount);
void getCanonicPrimeFactors(int n, PrimeFactor** factors, int* factorsCount);
