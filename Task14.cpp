
#include "stdafx.h"
#include "Task14.h"

/*
Euclidean algorithm for GCD
*/

int calculateGcdRecursve(int a, int b)
{
	int greater = __max(a, b);
	int lesser = __min(a, b);

	int reminder = greater % lesser;

	if (reminder == 0)
	{
		return lesser;
	}
	else
	{
		return calculateGcdRecursve(lesser, reminder);
	}
}

int calculateGcd(int a, int b)
{
	int reminder;

	for(;;)
	{
		reminder = (a > b) ? a % b : b % a;

		if (reminder == 0)
		{
			return __min(a, b);
		}
		else
		{
			a = __min(a, b);
			b = reminder;
		}
	}
}
