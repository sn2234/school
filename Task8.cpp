
#include "stdafx.h"

#include "Task8.h"

/*
	a*x^2+b*x+c == c + x*(b + x*(a))

	coefficients = [c, b, a]
*/
double calculateHornerRecursive(const double* coefficients, int nCoefficients, double x)
{
	if (nCoefficients > 1)
	{
		return coefficients[0] + x * calculateHornerRecursive(coefficients + 1, nCoefficients - 1, x);
	}
	else
	{
		return coefficients[0];
	}
}

double calculateHorner(const double* coefficients, int nCoefficients, double x)
{
	assert(nCoefficients > 0);

	double result = coefficients[nCoefficients-1];

	for (int i = nCoefficients - 2; i >= 0; i--)
	{
		result = result * x + coefficients[i];
	}

	return result;
}
