
#include "stdafx.h"
#include "Task16.h"

// Bit map, zero means prime number
typedef struct EratosphenSieve
{
	int length;
	unsigned char *data;
}EratosphenSieve;

void initSieve(EratosphenSieve* sieve, int maxNum)
{
	sieve->length = (maxNum / 8) + (maxNum % 8 == 0 ? 0 : 1);
	sieve->data = (unsigned char *)malloc(sieve->length * sizeof(unsigned char));

	memset(sieve->data, 0, sieve->length);
}

void deleteSieve(EratosphenSieve* sieve)
{
	free(sieve->data);
}

void setNumber(EratosphenSieve* sieve, int n)
{
	if (n == 0)
	{
		return;
	}

	n = n - 1;

	int bytePos = n / 8;
	int bitPos = n % 8;

	if (bytePos < sieve->length)
	{
		sieve->data[bytePos] |= 1 << bitPos;
	}
	else
	{
		assert(NULL);
	}
}

int findNextPrime(EratosphenSieve* sieve, int lastNum)
{
	// Find byte with zeros
	for (int bytePos = lastNum / 8; bytePos < sieve->length; bytePos++)
	{
		if (sieve->data[bytePos] != 0xFF)
		{
			// Find bit number
			for (int bitPos = 0; bitPos < 8; bitPos++)
			{
				if (bytePos * 8 + bitPos + 1 > lastNum)
				{
					if ((sieve->data[bytePos] & (1 << bitPos)) == 0)
					{
						return bytePos * 8 + bitPos + 1;
					}
				}
			}
		}
	}

	// No more primes
	return -1;
}

void fillMultiples(EratosphenSieve* sieve, int n)
{
	if (n == 1)
	{
		return;
	}

	int maxN = sieve->length * 8;

	int i = 2;
	while(n*i <= maxN)
	{
		setNumber(sieve, n*i);
		i++;
	}
}

void getPrimeNumbers(int maxNumber, int** primes, int* numPrimes)
{
	EratosphenSieve sieve = { 0 };

	int capacity = 100;
	int primesCount = 0;
	int* primeNumbers = (int*)malloc(capacity * sizeof(int));

	// Init sieve
	initSieve(&sieve, maxNumber);

	// Fill sieve
	int lastPrimeNumber = 1;

	while (lastPrimeNumber > 0 && lastPrimeNumber <= maxNumber)
	{
		if (primesCount >= capacity)
		{
			capacity *= 2;
			primeNumbers = (int*)realloc(primeNumbers, capacity * sizeof(int));

			if (primeNumbers == NULL)
			{
				*primes = NULL;
				*numPrimes = 0;

				return;
			}
		}

		primeNumbers[primesCount] = lastPrimeNumber;
		primesCount++;

		//setNumber(&sieve, lastPrimeNumber);
		fillMultiples(&sieve, lastPrimeNumber);
		lastPrimeNumber = findNextPrime(&sieve, lastPrimeNumber);
	}

	*primes = primeNumbers;
	*numPrimes = primesCount;

	deleteSieve(&sieve);
}
