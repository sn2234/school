
#include "stdafx.h"

#include "Task13.h"


int calcFibonacci(int n)
{
	int tmp = 0;

	int f0 = 0;
	int f1 = 1;

	for (int i = 0; i < n; i++)
	{
		tmp = f0 + f1;
		f0 = f1;
		f1 = tmp;
	}

	return f0;
}
