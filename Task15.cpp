
#include "stdafx.h"
#include "Task15.h"


// Returns list of all non-trivial dividers
void getRawFactors(int n, int** factors, int* factorsCount)
{
	int divider = 2;
	int maxDivider = (int)(sqrtf((float)n) + 1);
	int origN = n;

	int capacity = 10;
	int currentFactors = 0;
	int* factorsTmp = (int*)malloc(capacity * sizeof(int));

	while (1)
	{
		if (n % divider == 0)
		{
			// Add divider to list, increasing it if needed
			if (currentFactors == capacity)
			{
				capacity *= 2;

				factorsTmp = (int*)realloc(factorsTmp, capacity * sizeof(int));

				if (factorsTmp == NULL)
				{
					*factors = NULL;
					*factorsCount = 0;

					return;
				}
			}

			factorsTmp[currentFactors] = divider;
			currentFactors++;

			n /= divider;
		}
		else
		{
			divider++;

			if (divider > maxDivider)
			{
				break;
			}
		}
	}

	// If number is prime add itself to the results
	if (currentFactors == 0)
	{
		factorsTmp[0] = origN;
		currentFactors = 1;
	}

	*factors = factorsTmp;
	*factorsCount = currentFactors;
}

void getCanonicPrimeFactors(int n, PrimeFactor** factors, int* factorsCount)
{
	// Factorize number
	int* rawFactors = NULL;
	int rawFactorsNum = 0;

	getRawFactors(n, &rawFactors, &rawFactorsNum);

	if (rawFactors == NULL)
	{
		*factors = NULL;
		*factorsCount = 0;
	}


	// Fold raw factors to canonical view
	int factorsTmpCount = 0;
	int capacity = 10;
	PrimeFactor* factorsTmp = (PrimeFactor*)malloc(capacity * sizeof(PrimeFactor));

	PrimeFactor tmp = { 0 };

	for (int i = 0; i < rawFactorsNum; i++)
	{
		if (rawFactors[i] == tmp.factor)
		{
			tmp.count++;
		}
		else if(tmp.factor == 0)
		{
			// First time
			tmp.factor = rawFactors[i];
			tmp.count = 1;
		}
		else
		{
			// Finalize current factor, we've found the new one
			if (factorsTmpCount == capacity)
			{
				// Reallocate buffer
				capacity *= 2;

				factorsTmp = (PrimeFactor*)realloc(factorsTmp, capacity * sizeof(PrimeFactor));

				if (factorsTmp == NULL)
				{
					*factors = NULL;
					*factorsCount = 0;

					return;
				}
			}

			factorsTmp[factorsTmpCount] = tmp;
			factorsTmpCount++;

			tmp.count = 1;
			tmp.factor = rawFactors[i];
		}
	}

	if (tmp.factor != 0)
	{
		if (factorsTmpCount == capacity)
		{
			// Reallocate buffer
			capacity *= 2;

			factorsTmp = (PrimeFactor*)realloc(factorsTmp, capacity * sizeof(PrimeFactor));

			if (factorsTmp == NULL)
			{
				*factors = NULL;
				*factorsCount = 0;

				return;
			}
		}

		factorsTmp[factorsTmpCount] = tmp;
		factorsTmpCount++;

	}

	*factors = factorsTmp;
	*factorsCount = factorsTmpCount;
}
