// School.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Task8.h"
#include "Task13.h"
#include "Task14.h"
#include "Task15.h"
#include "Task16.h"

int _tmain(int argc, _TCHAR* argv[])
{
	double c[] = { 1.0, 2.0, 3.0, 4.0 };

	calculateHorner(c, sizeof(c) / sizeof(c[0]), 24.0);
	calculateHornerRecursive(c, sizeof(c) / sizeof(c[0]), 24.0);

	calcFibonacci(5);

	calculateGcdRecursve(1012, 16);
	calculateGcd(1012, 16);

	int testNumber = 2 * 2 * 2 * 23 * 19 * 13 * 11 * 5 * 3 * 7 * 17;
	{
		int* factors = NULL;
		int factorsNum = 0;

		getRawFactors(testNumber, &factors, &factorsNum);

		printf("\nFactors: ");
		for (int i = 0; i < factorsNum; i++)
		{
			printf("%d ", factors[i]);
		}

		free(factors);
	}

	{
		PrimeFactor* factors = NULL;
		int factorsCount = 0;

		getCanonicPrimeFactors(testNumber, &factors, &factorsCount);

		printf("\nCanonical factors: ");
		for (int i = 0; i < factorsCount; i++)
		{
			printf("%d^%d ", factors[i].factor, factors[i].count);
		}

		free(factors);
	}

	{
		int* primes = NULL;
		int numPrimes = 0;

		getPrimeNumbers(1000, &primes, &numPrimes);

		printf("\nPrimes: ");
		for (int i = 0; i < numPrimes; i++)
		{
			printf("%d ", primes[i]);
		}
	}

	printf("\n");

	return 0;
}
