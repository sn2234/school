#pragma once

double calculateHornerRecursive(const double* coefficients, int nCoefficients, double x);
double calculateHorner(const double* coefficients, int nCoefficients, double x);
